# -*- coding: utf-8 -*-
"""
Created on Thu Jan 25 13:17:42 2018
Using word2vec for doc similarity
word2vec from wa strats

Purpose: Calculate the similarity distance between documents using pre-trained word2vec model.
Python 3
gensim : to load the word2vec model
numpy : to calculate similarity scores

 tasks:
Function: display_closestwords_tsnescatterplot
1. Loads a pre-trained word2vec embedding
2. Finds similar words and appends each of the similar words embedding vector to the matrix
3. Applies TSNE to the Matrix to project each word to a 2D space (i.e. dimension reduction)
4. Plots the 2D position of each word with a label

Finction: 
1. Load a pre-trained word2vec model
2. Pass to DocSim class to calculate document similarities
3. Calculate the similarity score between a source document & a list of target documents
Note: You can optionally pass a threshold argument to the calculate_similarity() method to return only the target documents with similarity score above the threshold.
sim_scores = ds.calculate_similarity(source_doc, target_docs, threshold=0.7)

@author: 20230326
"""
from gensim.models.keyedvectors import KeyedVectors
from DocSim import DocSim

from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
import numpy as np


def display_closestwords_tsnescatterplot(model, word):
    
    arr = np.empty((0,100), dtype='f')
    word_labels = [word]

    # get close words
    close_words = model.similar_by_word(word)
    
    # add the vector for each of the closest words to the array
    arr = np.append(arr, np.array([model[word]]), axis=0)
    for wrd_score in close_words:
        wrd_vector = model[wrd_score[0]]
        word_labels.append(wrd_score[0])
        arr = np.append(arr, np.array([wrd_vector]), axis=0)
        
    # find tsne coords for 2 dimensions
    tsne = TSNE(n_components=2, random_state=0)
    np.set_printoptions(suppress=True)
    Y = tsne.fit_transform(arr)

    x_coords = Y[:, 0]
    y_coords = Y[:, 1]
    # display scatter plot
    plt.scatter(x_coords, y_coords)

    for label, x, y in zip(word_labels, x_coords, y_coords):
        plt.annotate(label, xy=(x, y), xytext=(0, 0), textcoords='offset points')
    plt.xlim(x_coords.min()+0.00005, x_coords.max()+0.00005)
    plt.ylim(y_coords.min()+0.00005, y_coords.max()+0.00005)
    plt.show()
    
    
def tsne_plot(model):
    "Creates and TSNE model and plots it"
    labels = []
    tokens = []

    for word in model.wv.vocab:
        tokens.append(model[word])
        labels.append(word)
    
    tsne_model = TSNE(perplexity=40, n_components=2, init='pca', n_iter=2500, random_state=23)
    new_values = tsne_model.fit_transform(tokens)

    x = []
    y = []
    for value in new_values:
        x.append(value[0])
        y.append(value[1])
        
    plt.figure(figsize=(16, 16)) 
    for i in range(len(x)):
        plt.scatter(x[i],y[i])
        plt.annotate(labels[i],
                     xy=(x[i], y[i]),
                     xytext=(5, 2),
                     textcoords='offset points',
                     ha='right',
                     va='bottom')
    plt.savefig("plot.pdf")
    plt.savefig("plot.png")
    plt.show()
    
    
    
# Using the pre-trained word2vec model trained using WA strat units file.

#stopwords_path = "./data/stopwords_en.txt"

model = KeyedVectors.load_word2vec_format('gensim_model_stratigraphy_min10.bin', binary=True)

" too much nodes here"
tsne_plot(model)



display_closestwords_tsnescatterplot(model, 'rock')
display_closestwords_tsnescatterplot(model, 'sandstone')

#with open(stopwords_path, 'r') as fh:
#    stopwords = fh.read().split(",")

#ds = DocSim(model,stopwords=stopwords)
ds = DocSim(model)

# Text similarity check
source_doc = "how to delete a rock and sandstone"
target_docs = ['delete a rock', 'how do i remove a sandstone', "purge an sedimentary rocks"]

# Compare documents
with open('file1.json', 'r') as file:
    source_doc = file.read()

with open('file2.json', 'r') as file:
    target_docs = [file.read()]
with open('file3.json', 'r') as file:
    target_docs.append(file.read())
with open('file4.json', 'r') as file:
    target_docs.append(file.read())
with open('file5.json', 'r') as file:
    target_docs.append(file.read())


sim_scores = ds.calculate_similarity(source_doc, target_docs)
#sim_scores = ds.calculate_similarity(source_doc, target_docs, threshold=0.7)
print(sim_scores)

#str_json = json.dumps(sim_scores)

with open('Document_Similarity_Result.txt', 'w') as file:
    file.write(str(sim_scores))


# Prints:
##  [{'score': 0.86057132, 'doc': 'delete a rock'},
# {'score': 0.5954926, 'doc': 'how do i remove a sandstone'},
# {'score': 0.59237665, 'doc': 'purge an sedimentary rocks'}]