"""
Created on Tue Jan 23 08:58:54 2018

Creates Neo4J Database from
C:\\wamex\WACurrent Stratigraphic Unit Data\WA Stratigraphic names Current_Shortened.xlsx

@author: 20230326
"""

class StratigraphicUnit:
    def __init__(self, stratNo, stratName, rank, parentStratNo, chemicalParentStratNo,
                 minAge, maxAge, lithologyGroup):
        self.stratNo = stratNo
        self.stratName = stratName
        self.rank = rank
        self.parentStratNo = parentStratNo
        self.chemicalParentStratNo = chemicalParentStratNo
        self.minAge = minAge
        self.maxAge = maxAge
        self.lithologyGroup = lithologyGroup
    
    def __str__(self):
        return("stratNo = {0}\n"
               "  stratName = {1}\n"
               "  rank = {2}\n"
               "  parentStratNo = {3}\n"
               "  chemicalParentStratNo = {4}\n"
               "  minAge = {5} \n"
               "  minAge = {6} \n"
               "  lithologyGroup = {7}"
               .format(self.stratNo, self.stratName, self.rank, self.parentStratNo,
                       self.chemicalParentStratNo, self.minAge, self.maxAge, self.lithologyGroup))

import xlrd

# Reading data
book = xlrd.open_workbook(r'C:\\wamex\WACurrent Stratigraphic Unit Data\WA Stratigraphic names Current_Shortened.xlsx')
sheet = book.sheet_by_index(0)

number_of_rows = sheet.nrows
number_of_columns = sheet.ncols
print(str(number_of_rows) + " " + str(number_of_columns))

StratUnits = []
stratNo = ''
stratName = ''
rank = ''
parentNo = ''
chemicalNo = ''
minAge = ''
maxAge = ''
lithGroup = ''

for row in range(1, number_of_rows):
    try:
        stratNo = int(sheet.cell(row, 1).value)
        stratName = sheet.cell(row, 0).value
        rank = sheet.cell(row, 2).value
        if (sheet.cell(row, 4).value == ''):
            parentNo = ''
        else:
            parentNo = int(sheet.cell(row, 4).value)
        if (sheet.cell(row, 6).value == ''):
            chemicalNo = ''
        else:
            chemicalNo = int(sheet.cell(row, 6).value)
        minAge = sheet.cell(row, 7).value
        maxAge = sheet.cell(row, 8).value
        lithGroup = sheet.cell(row, 9).value
        unit = StratigraphicUnit(stratNo, stratName, rank,
                                 parentNo, chemicalNo,
                                 minAge, maxAge, lithGroup)
        print(unit.__str__())
        StratUnits.append(unit)
    except ValueError:
        print(ValueError)
        pass

import jsonpickle
with open('WA_Stratigraphy_Detailed.json', 'w') as f:
            json = jsonpickle.encode(StratUnits, unpicklable=False)
            f.write(json) 

# show in graph
# libraries
from py2neo import authenticate, Graph

# set up authentication parameters
authenticate("localhost:7474", "neo4j", "wamex")
graph = Graph()
graph.delete_all()

from py2neo import Node, Relationship

# create important nodes
superGroup = Node("Rank", name = "SuperGroup")
graph.create(superGroup)
group = Node("Rank", name = "Group")
graph.create(group)
subgroup = Node("Rank", name = "SubGroup")
graph.create(subgroup)
formation = Node("Rank", name = "Formation")
graph.create(formation)
member = Node("Rank", name = "Member")
graph.create(member)
bed = Node("Rank", name = "Bed")
graph.create(bed)

graph.create(Relationship(group, "isChild", superGroup))
graph.create(Relationship(subgroup, "isChild", group))
graph.create(Relationship(formation, "isChild", subgroup))
graph.create(Relationship(member, "isChild", formation))
graph.create(Relationship(bed, "isChild", member))


# create nodes
for unit in StratUnits:
    aNode = Node("StratigraphicUnit", stratNo = unit.stratNo,
                 stratName = unit.stratName,
                 rank = unit.rank,
                 parentStratNo = unit.parentStratNo,
                 chemicalParentStratNo = unit.chemicalParentStratNo,
                 minAge = unit.minAge,
                 maxAge = unit.maxAge,
                 lithologyGroup = unit.lithologyGroup)
    graph.create(aNode)

childID = ''
parentID = ''

# create relations
for unit in StratUnits:
    if unit.parentStratNo != '':
        #childRelation = Relationship(unit.stratNo, "isChild", unit.parentStratNo)
        #graph.create(childRelation)
        childID = unit.stratNo
        parentID = unit.parentStratNo
        graph.run("MATCH (a:StratigraphicUnit{stratNo:" + str(childID) +
                                                         "}), (b:StratigraphicUnit{stratNo:" + str(parentID) +
                                                         "}) CREATE (a)-[:isChild]->(b)")
# create chemical parent child relations
for unit in StratUnits:
    if unit.chemicalParentStratNo != '':
        #childRelation = Relationship(unit.stratNo, "isChild", unit.parentStratNo)
        #graph.create(childRelation)
        childID = unit.stratNo
        parentID = unit.chemicalParentStratNo
        graph.run("MATCH (a:StratigraphicUnit{stratNo:" + str(childID) +
                                                         "}), (b:StratigraphicUnit{stratNo:" + str(parentID) +
                                                         "}) CREATE (a)-[:isChemicalChild]->(b)")

# create rank relations
for unit in StratUnits:
    if unit.rank == 'Supergroup':
        childID = unit.stratNo
        graph.run("MATCH (a:StratigraphicUnit{stratNo:" + str(childID) +
                                              "}), (b:Rank{name: \"SuperGroup\"})" + " CREATE (a)-[:rankOf]->(b)")
    if unit.rank == 'Group, Suite':
        childID = unit.stratNo
        graph.run("MATCH (a:StratigraphicUnit{stratNo:" + str(childID) +
                                              "}), (b:Rank{name: \"Group\"})" + " CREATE (a)-[:rankOf]->(b)")
    if unit.rank == 'Subgroup':
        childID = unit.stratNo
        graph.run("MATCH (a:StratigraphicUnit{stratNo:" + str(childID) +
                                              "}), (b:Rank{name: \"SubGroup\"})" + " CREATE (a)-[:rankOf]->(b)")
    if unit.rank == 'Formation, beds':
        childID = unit.stratNo
        graph.run("MATCH (a:StratigraphicUnit{stratNo:" + str(childID) +
                                              "}), (b:Rank{name: \"Formation\"})" + " CREATE (a)-[:rankOf]->(b)")
    if unit.rank == 'Member, phase':
        childID = unit.stratNo
        graph.run("MATCH (a:StratigraphicUnit{stratNo:" + str(childID) +
                                              "}), (b:Rank{name: \"Member\"})" + " CREATE (a)-[:rankOf]->(b)")
    if unit.rank == 'Bed':
        childID = unit.stratNo
        graph.run("MATCH (a:StratigraphicUnit{stratNo:" + str(childID) +
                                              "}), (b:Rank{name: \"Bed\"})" + " CREATE (a)-[:rankOf]->(b)")
    

    
#MATCH (a:Artist),(b:Album)
#WHERE a.Name = "Strapping Young Lad" AND b.Name = "Heavy as a Really Heavy Thing"
#CREATE (a)-[r:RELEASED]->(b)
    
# CREATE (le:Person {name:"Euler"}), (db:Person {name:"Bernoulli"}),
# (le)-[:KNOWS {since:1768}]->(db)
# RETURN le, db




