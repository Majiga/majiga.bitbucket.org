# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 08:19:25 2018

Creates word2vec model file:
gensim_model_stratigraphy.model
gensim_model_stratigraphy.bin

@author: 20230326
"""
import os, logging
import pandas as pd
import gensim
from gensim import models, similarities
import nltk
from nltk.stem import WordNetLemmatizer

os.chdir("H:\majiga.bitbucket.org\WA Stratigraphy")

STOP_WORDS = nltk.corpus.stopwords.words()
lemmatizer = WordNetLemmatizer()
sentences = []
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


def Prepare_Sentences():
    
    # Simple format: one sentence = one line; words already preprocessed and separated by whitespace.
# source can be either a string or a file object. Clip the file to the first limit lines (or not clipped if limit is None, the default).
#    sentences = LineSentence('myfile.txt')
#    sentences = LineSentence('compressed_text.txt.bz2')
#    sentences = LineSentence('compressed_text.txt.gz')

    # Works like word2vec.LineSentence, but will process all files in a directory in alphabetical order by filename
    # sentences = PathLineSentences(os.getcwd() + '\corpus\')

    #os.chdir("C:\wamex\WACurrent Stratigraphic Unit Data")
    df = pd.read_excel('WA Stratigraphic names Current_Shortened.xlsx')
    data = df['Stratigraphic Name '].values.tolist()
    changed_words = []
    changed_w = ''

    for row in data:
        try:
            print(row)
            row_lowercase = row.lower()
            words = row_lowercase.strip().split()        
            changed_words = []
            for w in words:                
                print(w)
#                if w in STOP_WORDS:
#                    break
#                if (w.isDigit()):
#                    continue
                
                changed_w = lemmatizer.lemmatize(w)
                changed_words.append(changed_w)                
                print(changed_w)            
            if (len(changed_words) > 0):
                sentences.append(changed_words)
        except:
            print("Error!")

def Create_Gensim_Model():
    # Gensim model for Word2Vec
    # train word2vec on the sentences
    #model = gensim.models.Word2Vec(sentences, size=100, window=5, min_count=1, workers=4)
    model = gensim.models.Word2Vec(sentences, size=100, window=5, min_count=1)
    model.wv.save('gensim_model_stratigraphy.model')
    model.wv.save_word2vec_format('gensim_model_stratigraphy.bin', binary=True)

    model = gensim.models.Word2Vec(sentences, size=100, window=5, min_count=3)
    model.wv.save_word2vec_format('gensim_model_stratigraphy_min3.bin', binary=True)

    model = gensim.models.Word2Vec(sentences, size=100, window=5, min_count=5)
    model.wv.save_word2vec_format('gensim_model_stratigraphy_min5.bin', binary=True)

    model = gensim.models.Word2Vec(sentences, size=100, window=5, min_count=10)
    model.wv.save_word2vec_format('gensim_model_stratigraphy_min10.bin', binary=True)

    return model

def Load_Gensim_Model():
    model = gensim.models.Word2Vec.load('gensim_model_stratigraphy.model')
    return model
    

#   
Prepare_Sentences()

model = Create_Gensim_Model()
# OR
#model = Load_Gensim_Model()
#model.train(more_sentences)

#model.accuracy('/tmp/questions-words.txt')


word_vectors = model.wv

# Finished training a model (no more updates, only querying),
#  then switch to the gensim.models.KeyedVectors instance in wv
del model

#print("Word Vectors Length: " + len(word_vectors.vocab.keys()))

#The word vectors are stored in a KeyedVectors instance in model.wv.
#This separates the read-only word vector lookup operations in KeyedVectors from the training code in Word2Vec
print(list(word_vectors['rock'])) # numpy vector of a word

# NLP word tasks with the model
#word_vectors.most_similar(positive=['woman', 'king'], negative=['man'])
#print(model.wv.most_similar(positive=['rock', 'stone'], negative=['sand']))

#word_vectors.doesnt_match("breakfast cereal dinner lunch".split())

print(word_vectors.similarity('sandstone', 'rock'))

print(word_vectors.most_similar('rock'))

#bigram_transformer = gensim.models.Phrases(sentences)
#model = Word2Vec(bigram_transformer[sentences], size=100, ...)

# Probability of a text under the model:
#model.score(["The fox jumped over a rock dog".split()])

# Correlation with human opinion on word similarity:
# model.wv.evaluate_word_pairs(os.path.join(module_path, 'test_data','wordsim353.tsv'))

# Correlation with human opinion on analogies
# word_vectors.accuracy(os.path.join(module_path, 'test_data', 'questions-words.txt'))

