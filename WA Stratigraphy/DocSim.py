# -*- coding: utf-8 -*-
"""
Copied on Thu Jan 25 15:57:56 2018 from
https://github.com/v1shwa/document-similarity
https://github.com/v1shwa/document-similarity/blob/master/DocSim.py
@author: 20230326
"""

import numpy as np

class DocSim(object):
    def __init__(self, w2v_model , stopwords=[]):
        self.w2v_model = w2v_model
        self.stopwords = stopwords

    def vectorize(self, doc):
        """Identify the vector values for each word in the given document"""
        doc = doc.lower()
        words = [w for w in doc.split(" ") if w not in self.stopwords]
        word_vecs = []
        for word in words:
            try:
                vec = self.w2v_model[word]
                word_vecs.append(vec)
            except KeyError:
                # Ignore, if the word doesn't exist in the vocabulary
                pass

        # Assuming that document vector is the mean of all the word vectors
        # PS: There are other & better ways to do it.
        vector = np.mean(word_vecs, axis=0)
        return vector


    def _cosine_sim(self, vecA, vecB):
        """Find the cosine similarity distance between two vectors."""
        csim = np.dot(vecA, vecB) / (np.linalg.norm(vecA) * np.linalg.norm(vecB))
        if np.isnan(np.sum(csim)):
            return 0
        return csim

    def calculate_similarity(self, source_doc, target_docs=[], threshold=0, printDoc=False):
        """Calculates & returns similarity scores between given source document & all
        the target documents."""
        if isinstance(target_docs, str):
            target_docs = [target_docs]

        source_vec = self.vectorize(source_doc)
        results = []
        count = 0
        for doc in target_docs:
            target_vec = self.vectorize(doc)
            sim_score = self._cosine_sim(source_vec, target_vec)
            if sim_score > threshold:
                if printDoc:
                    results.append({
                            'score' : sim_score,
                            'doc' : doc,
                            'docNum' : count
                            })
                else:
                    results.append({
                            'score' : sim_score,
                            'doc' : "Not showing as requested",
                            'docNum' : count
                            })
        
            count = count + 1
            # Sort results by score in desc order
            results.sort(key=lambda k : k['score'] , reverse=True)

        return results