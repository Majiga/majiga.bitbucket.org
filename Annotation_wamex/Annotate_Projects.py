
# coding: utf-8

# In[72]:


import nltk
import json

sentences = []
annotated_sentences = []

eras = []
minerals = []
rocks = []
commodities = []
stratigraphy = []
locations = []
projects = []

# ROCKS
with open(r'H:\majiga.bitbucket.org\data\Rock_Names.json', 'r') as f:
    text = f.read()
    text = text.lower()
    rocks = json.loads(text)
#print(rocks)
print(rocks[0])


# In[122]:


# MINERALS
with open(r'H:\majiga.bitbucket.org\data\Mineral_Names.json', 'r') as f:
    text = f.read()
    text = text.lower()
    minerals = json.loads(text)

#print(minerals)
print(minerals[0])


# In[74]:


# Commodity_Names
with open(r'H:\majiga.bitbucket.org\data\Commodity_Names.json', 'r') as f:
    text = f.read()
    text = text.lower()
    commodities = json.loads(text)
#print(v)
print(commodities[0])


# In[75]:


# ERAS
with open(r'H:\majiga.bitbucket.org\data\Geological_Eras.txt', 'r') as f:
    text = f.read()
    text = text.lower()
    eras = text.split(',')
eras[0]


# In[76]:


# LOCATIONS
#with open(r'H:\majiga.bitbucket.org\data\GeoLocation_Names_OZMIN.txt', 'r') as f:
#    text = f.read()
#    locations = text.split('\n')
#locations[0]


# In[77]:


# Project_Names
#with open(r'H:\majiga.bitbucket.org\data\Project_Names.json', 'r') as f:
#    projects = json.load(f)    
#projects[0]


# In[78]:


# STRATIGRAPHY
with open(r'H:\majiga.bitbucket.org\data\WA_Stratigraphic_Names.json', 'r') as f:
    text = f.read()
    text = text.lower()
    stratigraphy = json.loads(text)
#print(stratigraphy)
print(stratigraphy[0])


# In[79]:


data = ""
with open(r'H:\majiga.bitbucket.org\data\gsdmrb25_second_edition_Gemstones_of_WA.txt', 'r', encoding="utf8") as f:
    data = f.read()
data = data.replace("\n", " ")
data = data.lower()

# Sentences
sentences = nltk.sent_tokenize(data)
#print(sentences)


# In[87]:


class Annotation:
    
    def __init__(self, sentence, annotated_sentence, eras, rocks, minerals, commodities, stratigraphic_units):
        self.sentence = sentence
        self.annotated_sentence = annotated_sentence
        self.eras = eras
        self.rocks = rocks
        self.minerals = minerals
        self.commodities = commodities
        self.stratigraphic_units = stratigraphic_units
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)


# In[ ]:


def insert (source_str, insert_str, pos):
    return source_str[:pos]+insert_str+source_str[pos:]


# In[113]:


annotated_sentences = []

for s in sentences:
   
    s_low = s.lower()
    ann_sent = s_low
        
    s_eras = []
    s_rocks = []
    s_minerals = []
    s_commodities = []
    s_strats = []
    
    # Annotate with Geological timescale
    for era in eras:
        if (era.lower() in s_low):
            s_eras.append(era.lower())
            index = s_low.find(era.lower())
            #print("ERA: " + era)
    
    # Annotate with Rocks
    for rock in rocks:
        if (rock.lower() in  s_low):
            s_rocks.append(rock.lower())
            #print("ROCK: " + rock)

    # Annotate with minerals
    for m in minerals:
        if (m.lower() in s_low):
            s_minerals.append(m.lower())
            #print("MINERAL: " + m)

    # Annotate with Commodities
    for com in commodities:
        if (com.lower() in  s_low):
            s_commodities.append(com.lower())
            #print("COMMODITY: " + com)

    # Annotate with Geographical locations
    #for loc in locations:
    #    if (loc.lower() in s_low):
    #        print("LOCATION: " + loc)
            
    # Annotate with project names
    #for pr in projects:
    #    if (pr.lower() in s_low):
    #        print("PROJECT: " + pr)

    # Annotate with stratigraphic units data
    for strat in stratigraphy:
        if (strat.lower() in s_low):
            s_strats.append(strat.lower())
            #print("STRATIGRAPHY: " + strat)
            
    if ((s_eras is not None) and (len(s_eras) > 0) or
        (s_rocks is not None) and (len(s_rocks) > 0) or
        (s_minerals is not None) and (len(s_minerals) > 0) or
        (s_commodities is not None) and (len(s_commodities) > 0) or
        (s_strats is not None) and (len(s_strats) > 0)):
        annotation = Annotation(s, ann_sent, s_eras, s_rocks, s_minerals, s_commodities, s_strats);
        annotated_sentences.append(annotation)


# In[114]:


print(len(annotated_sentences))
for ann_sentence in annotated_sentences:
    
    if ((ann_sentence.rocks is not None) and (len(ann_sentence.rocks) > 0)):
        print(ann_sentence.rocks)
    if ((ann_sentence.minerals is not None) and (len(ann_sentence.minerals) > 0)):
        print(ann_sentence.minerals)


# In[90]:


import jsonpickle

print(json.dumps(json.loads(jsonpickle.encode(annotated_sentences)), indent=2))




# Remove stop words    
filtered_words = [word for word in data if word not in nltk.corpus.stopwords.words('english')]
print(data)


# In[86]:


import json
import codecs
with codecs.open('H:\majiga.bitbucket.org\data\Annotated.json', "w",encoding='utf-8', errors='ignore') as fp:
    json.dump(annotated_sentences, fp)

